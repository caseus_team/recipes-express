const { omit } = require("lodash");
const { sequelize, DataTypes } = require("./dbconfig");
const { Ingredient } = require("./ingredient");
const { Category } = require("./category");
const { RecipeIngredient } = require("./recipe_ingredient");

const Recipe = sequelize.define(
  "recipe",
  {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "id"
    },
    name: {
      type: DataTypes.STRING(1000),
      allowNull: true,
      field: "name"
    },
    briefDescription: {
      type: DataTypes.STRING(2000),
      allowNull: true,
      field: "brief_description"
    },
    description: {
      type: DataTypes.STRING(5000),
      allowNull: true,
      field: "description"
    }
  },
  {
    tableName: "recipe"
  }
);

Recipe.belongsTo(Category, { foreignKey: "category_id" });
Recipe.hasMany(RecipeIngredient, {
  as: "ingredients",
  foreignKey: "recipe_id"
});

const queryParameters = {
  attributes: ["id", "name", "briefDescription", "description"],
  include: [
    {
      model: RecipeIngredient,
      as: "ingredients",
      attributes: ["id", "amount"],
      include: [{ model: Ingredient }]
    },
    {
      model: Category
    }
  ]
};

const fetchAllInCategory = categoryId =>
  Recipe.findAll({
    where: { category_id: categoryId },
    ...queryParameters
  });

const fetch = id => Recipe.findByPk(id, queryParameters);

const destroy = id => Recipe.findByPk(id).then(recipe => recipe.destroy());

const update = initRecipe =>
  Recipe.findByPk(initRecipe.id, queryParameters)
    .then(recipe =>
      recipe.update({
        ...omit(initRecipe, ["ingredients", "category"]),
        category_id: initRecipe.category
      })
    )
    .then(recipe => {
      const { ingredients } = recipe;
      const { ingredients: initIngredients } = initRecipe;
      Promise.all(
        initIngredients.map(ingredient => {
          console.log(ingredient.ingredient);
          const { id } = ingredient;
          if (
            ingredients.find(recipeIngredient => recipeIngredient.id === id)
          ) {
            console.log("update");
            return RecipeIngredient.findByPk(id).then(recipeIngredient =>
              recipeIngredient.update({
                ...ingredient,
                ingredient_id: ingredient.ingredient,
                recipe_id: recipe.id
              })
            );
          } else {
            console.log("create");
            return RecipeIngredient.create({
              ...omit(ingredient, "ingredient"),
              ingredient_id: ingredient.ingredient,
              recipe_id: recipe.id
            });
          }
        })
      );
      ingredients
        .filter(
          recipeIngredient =>
            !initIngredients.find(
              ingredient => recipeIngredient.id === ingredient.id
            )
        )
        .forEach(recipeIngredient =>
          RecipeIngredient.findByPk(recipeIngredient.id).then(
            recipeIngredient => recipeIngredient.destroy()
          )
        );
      return fetch(recipe.id);
    });

const create = recipe =>
  Recipe.create().then(createdRecipe =>
    update({ ...recipe, id: createdRecipe.id })
  );

module.exports = {
  Recipe,
  fetchAllInCategory,
  fetch,
  destroy,
  update,
  create
};
