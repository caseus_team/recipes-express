const { Ingredient } = require("./ingredient");
const { sequelize, DataTypes } = require("./dbconfig");

const RecipeIngredient = sequelize.define(
  "recipeIngredient",
  {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "id"
    },
    amount: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: "amount"
    }
  },
  {
    tableName: "recipe_ingredient"
  }
);
RecipeIngredient.belongsTo(Ingredient, { foreignKey: "ingredient_id" });

module.exports = {
  RecipeIngredient
};
