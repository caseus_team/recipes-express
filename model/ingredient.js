const { sequelize, DataTypes } = require("./dbconfig");

const Ingredient = sequelize.define(
  "ingredient",
  {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "id"
    },
    name: {
      type: DataTypes.STRING(1000),
      allowNull: true,
      field: "name"
    }
  },
  {
    tableName: "ingredient"
  }
);

const fetchAll = () => Ingredient.findAll();

const save = ingredient => Ingredient.create({ ...ingredient });

module.exports = {
  Ingredient,
  fetchAll,
  save
};
