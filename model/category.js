const { sequelize, DataTypes } = require("./dbconfig");

const Category = sequelize.define(
  "category",
  {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "id"
    },
    name: {
      type: DataTypes.STRING(1000),
      allowNull: true,
      field: "name"
    },
    description: {
      type: DataTypes.STRING(5000),
      allowNull: true,
      field: "description"
    }
  },
  {
    tableName: "category"
  }
);

const fetchAll = () => Category.findAll();

const fetch = id => Category.findByPk(id);

module.exports = {
  Category,
  fetchAll,
  fetch
};
