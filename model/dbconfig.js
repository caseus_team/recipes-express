const Sequelize = require("sequelize");
var DataTypes = require("sequelize/lib/data-types");

const sequelize = new Sequelize("dbrecipes", "daria", "123456", {
  host: "localhost",
  dialect: "mysql",
  operatorsAliases: false,
  define: {
    timestamps: false
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

module.exports = {
  sequelize: sequelize,
  DataTypes: DataTypes
};
