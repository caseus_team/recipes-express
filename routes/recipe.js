const express = require("express");
const router = express.Router();
const recipe = require("../model/recipe");

router.get("/recipe/fetchAll/:categoryId/", function(req, res) {
  recipe
    .fetchAllInCategory(req.params.categoryId)
    .then(recipes => res.json(recipes));
});

router.get("/recipe/fetch/:id/", (req, res) => {
  recipe.fetch(req.params.id).then(recipe => res.json(recipe));
});

router.delete("/recipe/delete/:id/", (req, res) => {
  recipe.destroy(req.params.id).then(() => res.send());
});

router.patch("/recipe/save/:id/", (req, res) => {
  recipe.update(req.body).then(recipe => res.json(recipe));
});

router.post("/recipe/save/", (req, res) => {
  recipe.create(req.body).then(recipe => res.json(recipe));
});

router.post("/recipe/fetchAll/", function(req, res) {
  res.send('Не работаеть');
});

module.exports = router;
