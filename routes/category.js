const category = require("../model/category");
const express = require("express");

const router = express.Router();

router.get("/category/fetchAll/", function(req, res) {
  category.fetchAll().then(categories => res.json(categories));
});

router.get("/category/fetch/:id", function(req, res) {
  category.fetch(req.params.id).then(category => res.json(category));
});

module.exports = router;
