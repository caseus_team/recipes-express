const express = require("express");
const router = express.Router();
const ingredient = require("../model/ingredient");

router.get("/ingredient/fetchAll/", function(req, res) {
  ingredient.fetchAll().then(ingredients => res.json(ingredients));
});

router.post("/ingredient/save/", (req, res) => {
  ingredient.save(req.body).then(res.send());
});

module.exports = router;
